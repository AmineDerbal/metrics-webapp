import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

const initialState = {
  cryptos: [],
  isLoading: false,
  hasError: false,
  isFetched: false,
};

export const getCryptosData = createAsyncThunk(
  'crypto/getAllData',
  async () => {
    const options = {
      method: 'GET',
      headers: {
        'X-API-KEY': `${process.env.REACT_APP_API_KEY}`,
      },
    };
    try {
      const dataStream = await fetch(process.env.REACT_APP_API_URL, options);
      const data = await dataStream.json();
      return data.result;
    } catch (err) {
      return err;
    }
  },
);

const cryptoSlice = createSlice({
  name: 'crypto',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getCryptosData.pending, (state) => {
      const isLoading = true;
      return {
        ...state,
        isLoading,
      };
    });
    builder.addCase(getCryptosData.fulfilled, (state, action) => {
      const isLoading = false;
      const isFetched = true;
      const cryptos = action.payload;
      return {
        ...state,
        cryptos,
        isLoading,
        isFetched,
      };
    });
    builder.addCase(getCryptosData.rejected, (state) => {
      const isLoading = false;
      const hasError = true;
      return {
        ...state,
        isLoading,
        hasError,
      };
    });
  },
});

export default cryptoSlice.reducer;
